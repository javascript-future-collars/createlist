let shopList = ["Oranges", "Bread", "Coca-Cola"];
const createList = (shopList) => {
  const ul = document.createElement("ul");
  ul.id = "shop-list";
  shopList.map((item) => {
    const li = document.createElement("li");
    li.textContent = item;
    ul.appendChild(li);
    return document.body.appendChild(ul);
  });
};

createList(shopList);
